package Clases;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luis on 6/07/2017.
 */
public class Control {
    private Alumno[] listado;
    private int cantReal;
    private List<Cursos> listadoCursos;
    private List<Grados> listadoGrados;

    public Control (int cantAlumno){
        listado =new Alumno[cantAlumno];
        cantReal=0;
        listadoCursos=new ArrayList<>();
        listadoGrados=new ArrayList<>();
    }

    public Alumno[] getListado() {
        return listado;
    }
    public void setListado(Alumno[] listado) {
        this.listado = listado;
    }

    public int getCantReal() {
        return cantReal;
    }
    public void setCantReal(int cantReal) {
        this.cantReal = cantReal;
    }

    public List<Cursos> getListadoCursos() {
        return listadoCursos;
    }


    public List<Grados> getListadoGrados() {
        return listadoGrados;
    }
    public void setListadoGrados(List<Grados> listadoGrados) {
        this.listadoGrados = listadoGrados;
    }

    public void adicionarAlumno (Alumno a) throws   Exception{
        if (cantReal<listado.length){
            listado[cantReal] = a;
            cantReal++;
        }
        else
            throw new Exception("Imposible agregar alumnos");
    }

    public void adicionarCurso (Cursos c){
        listadoCursos.add(c);
    }

    public void adicionarGrado (Grados g){
        listadoGrados.add(g);
    }
}
