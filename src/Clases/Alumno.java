package Clases;

/**
 * Created by Luis on 6/07/2017.
 */
public class Alumno {
    private String Nombre;
    private String Direccion;
    private int Edad;

    public Alumno() {
    }

    public Alumno(String nombre) {
       this.Nombre = nombre;
    }
    public Alumno(int edad) {
        this.Edad = edad;
    }

    public String getNombre() {
        return Nombre;
    }

    public String getDireccion() {
        return Direccion;
    }

    public int getEdad() {
        return Edad;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public void setEdad(int edad) {
        Edad = edad;
    }
}
