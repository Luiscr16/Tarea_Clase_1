package Clases;

/**
 * Created by Luis on 6/07/2017.
 */
public class Cursos {
    private String nombre;

    public Cursos(String nombre) {
        this.nombre = nombre;
    }

    public Cursos() {

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
